#include <thread>
#include <iostream>
#include <mutex>
#include <chrono>
#include <fstream>
#
using namespace std;
std::mutex reading;
std::mutex writing;
int countRead = 0;
int countWrite = 0;
bool isEmpty = false;//true if empty ,false if he isnt empty


void read(fstream* f)
{
	
	if (f&&!isEmpty)
	{
		writing.lock();
		reading.lock();//locked the resource , only one at a time can read
		cout << "locked reading" << endl;
		countRead++;
		cout << "reading from file"<<endl;
		isEmpty = true;
		countRead--;
		cout << "file is empty" << endl;
		cout << "unlocked reading" << endl;
		reading.unlock();//finish reading file is empty
		writing.unlock();
		
	}
	


}

void write(fstream* f)
{
	if (countRead == 0)
	{	
		reading.lock();
		cout << "no readers, locking writers" << endl;
		writing.lock();//locked the resource no reader ,one writing at a time
		cout << "locked writing " << endl;
		countWrite++;
		cout << "writing to file" << endl;
		isEmpty = false;
		countWrite--;
		cout << "file isnt empty anymore" << endl;
		writing.unlock();//finish writing file isnt empty anymore
		reading.unlock();
	}



}

int main()
{
	fstream* f; // shared resource
	thread t1(read,f);
	thread t2(write,f);

	t1.join();
	t2.join();

	return 0;
}